# deliverable

A server written with Typescript used for validating and formatting restaurant's weekly defined opening hours. See Testing with cURL on how to use.

## Running the project

Install dependencies:

```npm install```

Run the project:

```npm start```

The server should be running in port 3000 or in \$PORT.

## Testing

Run tests with:

```npm test```

### Testing with cURL:

The server can also be tested manually via cURL. In data/ folder there is one example json, one bad json, and formatted output of the example json for testing purposes.

Use good json (data/example.json)

```curl --data "@data/example.json" -H "Content-Type: application/json" -X POST -i "http://localhost:3000/format_opening_hours"```

or with bad json (data/bad_data.json):

```curl --data "@data/bad_data.json" -H "Content-Type: application/json" -X POST -i "http://localhost:3000/format_opening_hours"```

## Ponderings on the JSON format

The data format used is good for its purpose - being easy enough for an assignment. By its specification it is well defined and what I like about it is that it doesn't have nullable top-level fields, i.e. all days are defined (even though they could be empty). This, in my opinion, is easier to write a parsing algorithm for.

Writing the formatting and also the validation algorithm, a few thoughts came to my head about of a better format for the algorithms I came up with. The best one that came to my head was something like this:

```
{
  openingHours: [
    {
      "open": <time>,
      "close": <time>
    },
    {
      "open": <time>,
      "close": <time>
    },
    {
      "open": <time>,
      "close": <time>
    }
  ]
}
```

This would be easier to parse, since open and close pairs are always defined in the same element and it is just an array of opening hours. Everyday 8 AM - 9 PM schedule would just be 7 objects in the array, just like in the original format. However, time would atleast have to have max limit of week + 1 day instead of one day. 

I would also consider using proper unix time with timezones (instead of date being set at 1.1.1970). That would make updating single day's opening hours easier, and showing the time in different timezones easier. For example to help a customer with opening hours in Oslo from Finland, the program would either need to do timezone calculations itself, which is generally a bad choice (see time is hard https://speakerdeck.com/sinjo/time-is-hard), or it could just use good 3rd party libraries to do that if proper unix time was used.