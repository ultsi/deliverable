import { IOpeningHoursJson, IDayData } from './types';

const DAYS = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

export function getTimeIn12HFormat(inEpoch: number) {
  let hours = Math.floor(inEpoch / 3600);
  const minutes = Math.floor((inEpoch - hours * 3600) / 60);
  const seconds = inEpoch - hours * 3600 - minutes * 60;
  const period = hours >= 12 ? 'PM' : 'AM';
  hours = hours > 12 ? hours - 12 : hours;

  if (seconds === 0 && minutes === 0) {
    return `${hours} ${period}`;
  } else if (seconds === 0) {
    return `${hours}:${minutes} ${period}`;
  } else {
    return `${hours}:${minutes}:${seconds} ${period}`;
  }
}

function capitalizeFirstLetter(text: string) {
  return `${text.substring(0, 1).toUpperCase()}${text.substring(1)}`;
}

// TODO: try simplifying with .reduce
function figureOpenHoursForDay(
  dayData: IDayData[],
  dayName: string,
  dayIndex: number,
  json: IOpeningHoursJson,
): string {
  const openHours: string[] = [];
  let openHour = '';
  let open = false;
  dayData.forEach(statusChange => {
    if (statusChange.type === 'open') {
      open = true;
      openHour = getTimeIn12HFormat(statusChange.value);
    } else {
      if (!open) {
        // skip, part of day before
      } else {
        open = false;
        const closingHour = getTimeIn12HFormat(statusChange.value);
        openHours.push(`${openHour} - ${closingHour}`);
      }
    }
  });
  // handle the corner case that the restaurant is open after midnight
  if (open) {
    const nextDay = DAYS[(dayIndex + 1) % 7];
    const nextDayData = json[nextDay];
    const closeData = nextDayData[0];
    const closingHour = getTimeIn12HFormat(closeData.value);
    openHours.push(`${openHour} - ${closingHour}`);
    open = false;
  }
  return openHours.join(', ');
}

export function formatOpeningHoursJson(json: IOpeningHoursJson): string {
  const formatted: string[] = [];

  DAYS.forEach((dayName: string, index: number) => {
    const dayNameCapitalized = capitalizeFirstLetter(dayName);
    const dayData = json[dayName];
    const openHours = figureOpenHoursForDay(dayData, dayName, index, json);
    if (openHours) {
      formatted.push(`${dayNameCapitalized}: ${openHours}`);
    } else {
      formatted.push(`${dayNameCapitalized}: Closed`);
    }
  });

  return formatted.join('\n');
}
