import express = require('express');
import { IOpeningHoursJson, IFormatOpeningHoursResponse } from './types';
import bodyParser = require('body-parser');
import { validateOpeningHoursJson } from './validate';
import { formatOpeningHoursJson } from './format';
const app: express.Application = express();

const PORT = process.env.PORT || 3000;

app.use(bodyParser.json());

/* POST /format_opening_hours
  body: open hours as JSON (see data/example.json)
  returns open hours in human readable format
  see types.d.ts for response type definition (IFormatOpeningHoursResponse)
*/
app.post('/format_opening_hours', (req: express.Request, res: express.Response) => {
  const response: IFormatOpeningHoursResponse = { result: '', success: true };
  try {
    const openingHoursData: IOpeningHoursJson = req.body as IOpeningHoursJson;
    const validity = validateOpeningHoursJson(openingHoursData);
    if (!validity.ok) {
      response.success = false;
      response.errors = validity;
      response.errorMessage = 'Validation failed.';
      res.status(400);
      res.set('Content-Type', 'application/json');
      return res.send(JSON.stringify(response));
    }

    response.result = formatOpeningHoursJson(openingHoursData);
    return res.status(200).send(response);
  } catch (err) {
    switch (err.constructor) {
      case SyntaxError:
        // shouldn't happen since bodyParser seems to automatically convert bad data to empty json {}
        // but let's keep this here to be safe
        console.error(err);
        response.errorMessage = `Couldn't parse JSON!`;
        response.success = false;
        res.status(400).send(response);
        break;
      default:
        console.error(err);
        response.errorMessage = `Internal server error.`;
        response.success = false;
        res.status(500).send(response);
        break;
    }
  }
});

export default app.listen(PORT, () => {
  console.log(`App listening in port ${PORT}`);
});
