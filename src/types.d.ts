export interface IDayData {
  type: string;
  value: number;
}

export interface IOpeningHoursJson {
  monday: IDayData[];
  tuesday: IDayData[];
  wednesday: IDayData[];
  thursday: IDayData[];
  friday: IDayData[];
  saturday: IDayData[];
  sunday: IDayData[];
  [key: string]: IDayData[];
}

export interface IInvalidDayDataError {
  day: string;
  invalidIndex: number;
}

export interface IOpeningHoursJsonValidateResult {
  missingDays: string[];
  invalidDays: IInvalidDayDataError[];
  ok: boolean;
}

export interface IFormatOpeningHoursResponse {
  success: boolean;
  errors?: IOpeningHoursJsonValidateResult;
  errorMessage?: string;
  result: string;
}
