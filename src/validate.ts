import {
  IOpeningHoursJson,
  IOpeningHoursJsonValidateResult,
  IInvalidDayDataError,
  IDayData,
} from './types';

const DAYS = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
const DAY_LENGTH = 86400;

export function validateOpeningHoursJson(json: IOpeningHoursJson): IOpeningHoursJsonValidateResult {
  const result: IOpeningHoursJsonValidateResult = { missingDays: [], invalidDays: [], ok: true };

  let open = false;
  let openCount = 0;
  let closeCount = 0;
  DAYS.forEach((dayName, dayIndex) => {
    const dayData: IDayData[] = json[dayName];
    if (!dayData) {
      result.missingDays.push(dayName);
      result.ok = false;
      return;
    }

    const errors: IInvalidDayDataError[] = [];
    dayData.forEach((status: IDayData, index: number) => {
      if (status.type !== 'close' && status.type !== 'open') {
        result.invalidDays.push({ day: dayName, invalidIndex: index });
        result.ok = false;
      }
      if (status.value >= DAY_LENGTH) {
        result.invalidDays.push({ day: dayName, invalidIndex: index });
        result.ok = false;
      }
      if (status.type === 'open') {
        open = true;
        openCount += 1;
        closeCount = 0;
        if (openCount > 1) {
          // recognized 2 opens in a row. Should be in order of open,close pairs
          result.invalidDays.push({ day: dayName, invalidIndex: index });
          result.ok = false;
        }
      }
      if (status.type === 'close') {
        open = false;
        closeCount += 1;
        openCount = 0;
        if (closeCount > 1) {
          // recognized 2 opens in a row. Should be in order of open,close pairs
          result.invalidDays.push({ day: dayName, invalidIndex: index });
          result.ok = false;
        }
      }
    });
  });

  if (open) {
    // check that we have one last close on the first day
    if (!json.monday[0] || json.monday[0].type !== 'close') {
      // sunday is the invalid day with no closing time
      result.invalidDays.push({ day: 'sunday', invalidIndex: json.sunday.length - 1 });
      result.ok = false;
    }
  }

  return result;
}
