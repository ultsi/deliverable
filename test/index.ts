import chai from 'chai';
import app from '../src/index';
import 'mocha';
import chaiHttp = require('chai-http');
import { readFileSync } from 'fs';
import { getTimeIn12HFormat } from '../src/format';
const should = chai.should();
const expect = chai.expect;
chai.use(chaiHttp);

const goodJson = JSON.parse(readFileSync('data/example.json').toString());
const goodJson2 = JSON.parse(readFileSync('data/example2.json').toString());
const badJson = JSON.parse(readFileSync('data/bad_data.json').toString());

const goodJsonFormatted = readFileSync('data/example_json_formatted.txt').toString();
const goodJson2Formatted = readFileSync('data/example2_json_formatted.txt').toString();

describe('POST /format_opening_hours', () => {
  it('with no data, should respond status 400 with error json', done => {
    chai
      .request(app)
      .post('/format_opening_hours')
      .end((err, res) => {
        res.should.have.status(400);
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('success');
        res.body.success.should.equal(false);
        res.body.should.have.property('errors');
        res.body.errors.should.have.property('missingDays');
        res.body.errors.missingDays.should.contain('tuesday');
        res.body.errors.should.have.property('invalidDays');
        res.body.errors.invalidDays.should.be.empty;
        res.body.should.have.property('errorMessage');
        res.body.errorMessage.should.equal(`Validation failed.`);
        done();
      });
  });

  it('with bad data, should respond status 400 with different error json', done => {
    chai
      .request(app)
      .post('/format_opening_hours')
      .send('asdasdasd')
      .end((err, res) => {
        res.should.have.status(400);
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('success');
        res.body.success.should.equal(false);
        res.body.should.have.property('errors');
        res.body.errors.should.have.property('missingDays');
        res.body.errors.missingDays.should.contain('monday');
        res.body.errors.should.have.property('invalidDays');
        res.body.errors.invalidDays.should.be.empty;
        res.body.should.have.property('errorMessage');
        res.body.errorMessage.should.equal(`Validation failed.`);
        done();
      });
  });

  it('with bad json, should respond status 400 with specific errors', done => {
    chai
      .request(app)
      .post('/format_opening_hours')
      .send(badJson)
      .end((err, res) => {
        res.should.have.status(400);
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('success');
        res.body.success.should.equal(false);
        res.body.should.have.property('errors');
        res.body.errors.should.have.property('missingDays');
        res.body.errors.missingDays.should.include('friday');
        res.body.errors.should.have.property('invalidDays');
        res.body.errors.invalidDays.should.not.be.empty;
        expect(res.body.errors.invalidDays[0].day).to.equal('thursday');
        expect(res.body.errors.invalidDays[0].invalidIndex).to.equal(0);
        expect(res.body.errors.invalidDays[2].day).to.equal('saturday');
        expect(res.body.errors.invalidDays[2].invalidIndex).to.equal(1);
        res.body.should.have.property('errorMessage');
        res.body.errorMessage.should.equal(`Validation failed.`);
        done();
      });
  });

  it("with good json, should respond status 200 and return formatted result in 'result' field", done => {
    chai
      .request(app)
      .post('/format_opening_hours')
      .send(goodJson)
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('success');
        res.body.success.should.equal(true);
        res.body.should.have.property('result');
        res.body.result.should.equal(goodJsonFormatted);
        done();
      });
  });

  it("with good json 2, should respond status 200 and return formatted result in 'result' field", done => {
    chai
      .request(app)
      .post('/format_opening_hours')
      .send(goodJson2)
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('success');
        res.body.success.should.equal(true);
        res.body.should.have.property('result');
        res.body.result.should.equal(goodJson2Formatted);
        done();
      });
  });
});

describe('getTimeIn12HFormat', () => {
  it('should return 1 PM for hour 13', () => {
    expect(getTimeIn12HFormat(13 * 3600)).to.equal('1 PM');
  });

  it('should return 1:24:56 AM for time 5096', () => {
    expect(getTimeIn12HFormat(5096)).to.equal('1:24:56 AM');
  });
});
